/*
* @author Alberto Vilches
* @date 23/02/2018
*/
package dao;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import dto.SuperheroDTO;
import models.Superhero;
import ninja.jpa.UnitOfWork;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.*;
import java.util.stream.Collectors;

public class SuperheroDao {

    @Inject
    Provider<EntityManager> entitiyManagerProvider;

    @Transactional
    public Superhero addSuperhero(SuperheroDTO dto) {
        EntityManager entityManager = entitiyManagerProvider.get();
        Superhero hero = new Superhero();
        hero.setName(dto.getName());
        hero.setPseudonym(dto.getPseudonym());
        hero.setPublisher(dto.getPublisher());
        hero.setSkills(dto.getSkills().stream().distinct().collect(Collectors.toList())); // No duplicates

        dto.getAllies().stream().
                distinct(). // remove duplicates
                map(this::findByPseudonym).
                filter(Optional::isPresent).
                map(Optional::get).
                forEach(allied -> {
                    hero.addAllied(allied);
        });
        entityManager.persist(hero);

        return hero;
    }

    @UnitOfWork
    public Collection<SuperheroDTO> listAllSuperherosDTO() {
        return listSuperheros().stream().map(this::transformHeroToDto).collect(Collectors.toList());
    }

    @UnitOfWork
    public Optional<SuperheroDTO> findSuperheroDTO(String pseudonym) {
        return findByPseudonym(pseudonym).map(this::transformHeroToDto);
    }

    private SuperheroDTO transformHeroToDto(Superhero hero) {
        SuperheroDTO dto = new SuperheroDTO();
        dto.setName(hero.getName());
        dto.setPseudonym(hero.getPseudonym());
        dto.setPublisher(hero.getPublisher());
        dto.setSkills(hero.getSkills().stream().collect(Collectors.toList()));
        dto.setAllies(hero.getAllies().stream().map(Superhero::getPseudonym).collect(Collectors.toList()));
        return dto;
    }

    private Optional<Superhero> findByPseudonym(String pseudonym) {
        try {
            // WARNING: This search needs an index
            return Optional.of(entitiyManagerProvider.get().
                    createQuery("FROM Superhero where pseudonym = :pseudonym", Superhero.class).
                    setParameter("pseudonym", pseudonym).
                    setMaxResults(1).
                    getSingleResult());
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    private Collection<Superhero> listSuperheros() {
        return entitiyManagerProvider.get().
                createQuery("FROM Superhero x", Superhero.class).
                getResultList();
    }

}
