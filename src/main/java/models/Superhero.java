/*
* @author Alberto Vilches
* @date 22/02/2018
*/
package models;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "superhero")
public class Superhero {

    public Superhero() {}

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    Long id;

    private  String name;
    private  String pseudonym;
    private  String publisher;
    
    @ElementCollection(targetClass = String.class)
    @CollectionTable(name="skills", joinColumns=@JoinColumn(name="superhero_id"))
    private List<String> skills = new ArrayList<>();

    @ManyToMany(cascade = {
        CascadeType.PERSIST,
        CascadeType.MERGE
    })
    @JoinTable(name = "allies",
        joinColumns = @JoinColumn(name = "superhero_id"),
        inverseJoinColumns = @JoinColumn(name = "allied_id")
    )
    private List<Superhero> allies = new ArrayList<>();


    public void addAllied(Superhero allied) {
        this.getAllies().add(allied);
        allied.getAllies().add(this);
    }

    public void removeAllied(Superhero allied) {
        this.getAllies().remove(allied);
        allied.getAllies().remove(this);
    }

    public List<Superhero> getAllies() {
        return allies;
    }

    public void setAllies(List<Superhero> allies) {
        this.allies = allies;
    }

    public List<String> getSkills() {
        return skills;
    }

    public void setSkills(List<String> skills) {
        this.skills = skills;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPseudonym() {
        return pseudonym;
    }

    public void setPseudonym(String pseudonym) {
        this.pseudonym = pseudonym;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    @Override
    public String toString() {
        return "Superhero{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", pseudonym='" + pseudonym + '\'' +
                ", publisher='" + publisher + '\'' +
                '}';
    }
}