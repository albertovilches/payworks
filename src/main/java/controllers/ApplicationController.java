/**
 * Copyright (C) 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Copyright (C) 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package controllers;

import com.google.inject.Inject;
import com.google.inject.Provider;
import dao.SuperheroDao;
import dto.SuperheroDTO;
import models.Superhero;
import ninja.Result;
import ninja.Results;

import com.google.inject.Singleton;
import ninja.jpa.UnitOfWork;
import ninja.params.PathParam;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Singleton
public class ApplicationController {

    @Inject
    SuperheroDao superheroDao;

    public Result index() {
        return Results.html();
    }

    public Result getSuperhero(@PathParam("pseudonym") String pseudonym) {
        Optional<SuperheroDTO> dto = superheroDao.findSuperheroDTO(pseudonym);
        if (dto.isPresent()) {
            return Results.json().render(dto.get());
        }
        return Results.notFound().json().render(Collections.EMPTY_MAP);
    }

    public Result listSuperheros() {
        Collection<SuperheroDTO> dtos = superheroDao.listAllSuperherosDTO();
        return Results.json().render(dtos);
    }

    public Result addSuperhero(SuperheroDTO dto) {
        superheroDao.addSuperhero(dto);
        return Results.json().render(dto);
    }

}
