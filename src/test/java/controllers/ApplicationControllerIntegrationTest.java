/**
 * Copyright (C) 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Copyright (C) 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package controllers;


import dto.SuperheroDTO;
import org.junit.Test;

import ninja.NinjaDocTester;
import org.doctester.testbrowser.Request;
import org.doctester.testbrowser.Response;

import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class ApplicationControllerIntegrationTest extends NinjaDocTester {
    
    String URL_LIST = "/superheros";
    String URL_GET = "/superhero";

    @Test
    public void testListAlwaysReturnOkAndListJson() {
        Response response = makeRequest(
                Request.GET().url(
                        testServerUrl().path(URL_LIST)));
        assertEquals(response.httpStatus, HttpServletResponse.SC_OK);
        List json = response.payloadAs(List.class);
        assertNotNull(json);
    }
    
    @Test
    public void testGetSuperheroNotFound() {
        Response responseGet = makeRequest(
                Request.GET().url(
                        testServerUrl().path(URL_GET+"/pseudonymNotFound")));

        assertEquals(responseGet.httpStatus, HttpServletResponse.SC_NOT_FOUND);

        assertNotNull(responseGet.payloadAs(Map.class));

    }

    @Test
    public void testAddSuperhero() {

        SuperheroDTO model = createSuperheroDTOModel();

        Response responsePost = makePostRequestCreateSuperhero(model);

        assertEquals(responsePost.httpStatus, HttpServletResponse.SC_OK);

        assertEqualsToModel(responsePost.payloadAs(Map.class), model);

    }

    @Test
    public void testAddSuperheroThenGet() {

        SuperheroDTO model = createSuperheroDTOModel();

        makePostRequestCreateSuperhero(model);

        Response responseGet = makeRequest(
                Request.GET().url(
                        testServerUrl().path("/superhero/" + model.getPseudonym())));

        assertEquals(responseGet.httpStatus, HttpServletResponse.SC_OK);
        Map jsonGet = responseGet.payloadAs(Map.class);
        assertNotNull(jsonGet);

    }

    @Test
    public void testAddSuperheroAndList() {
        SuperheroDTO model = createSuperheroDTOModel();

        makePostRequestCreateSuperhero(model);

        Response responseList = makeRequest(
                Request.GET().url(
                        testServerUrl().path(URL_LIST)));

        List jsonList = responseList.payloadAs(List.class);

        Map jsonGet = (Map)jsonList.get(0);
        assertEqualsToModel(jsonGet, model);

    }

    private Response makePostRequestCreateSuperhero(SuperheroDTO model) {
        return makeRequest(Request.POST().url(
                        testServerUrl().path(URL_LIST)).
                        addFormParameter("name", model.getName()).
                        addFormParameter("publisher", model.getPublisher()).
                        addFormParameter("skills", model.getSkills().get(0)). // Ninja doesn't allow more than one parameter with the same name!!!
                        addFormParameter("pseudonym", model.getPseudonym()));
    }

    private SuperheroDTO createSuperheroDTOModel() {
        SuperheroDTO model = new SuperheroDTO();
        model.setName("Clark Kent");
        model.setPublisher("DC");
        model.setPseudonym("Superman");
        model.setName("Clark Kent");
        model.setSkills(Arrays.asList("x-ray"));
        return model;
    }

    private void assertEqualsToModel(Map json, SuperheroDTO model) {
        assertNotNull(json);
        assertEquals(json.get("name"), model.getName());
        assertEquals(json.get("publisher"), model.getPublisher());
        assertEquals(json.get("pseudonym"), model.getPseudonym());
        assertTrue(((List)json.get("skills")).containsAll(model.getSkills()));
    }

}
