/**
 * Copyright (C) 2012-2018 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p>
 * Copyright (C) 2013 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Copyright (C) 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package controllers;


import dao.SuperheroDao;
import dto.SuperheroDTO;
import models.Superhero;
import ninja.Result;
import ninja.Results;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationControllerMockUnitTest {


    @Mock
    SuperheroDao superheroDao;

    ApplicationController applicationController;

    @Before
    public void setupTest() {
        applicationController = new ApplicationController();
        applicationController.superheroDao = superheroDao;

    }

    public Result addSuperheros(SuperheroDTO dto) {
        superheroDao.addSuperhero(dto);
        return Results.json().render(dto);
    }

    @Test
    public void testAddSuperheroAlwaysReturnOkAndMap() {
        when(superheroDao.addSuperhero(null)) .thenReturn(new Superhero());
        SuperheroDTO dtoInput = new SuperheroDTO();
        Result result = applicationController.addSuperhero(dtoInput);

        assertEquals(result.getContentType(), "application/json");

        SuperheroDTO toRender = (SuperheroDTO)result.getRenderable();
        assertSame(dtoInput, toRender);

        assertEquals(result.getStatusCode(), HttpServletResponse.SC_OK);
    }

    @Test
    public void testListAlwaysReturnOkAndListJson() {
        when(superheroDao.listAllSuperherosDTO()) .thenReturn(Arrays.asList(new SuperheroDTO()));
        Result result = applicationController.listSuperheros();

        assertEquals(result.getContentType(), "application/json");
        assertTrue(!((Collection)result.getRenderable()).isEmpty());
        assertEquals(result.getStatusCode(), HttpServletResponse.SC_OK);
    }


    @Test
    public void testGetSuperHeroExists() {
        SuperheroDTO dtoFound = new SuperheroDTO();
        when(superheroDao.findSuperheroDTO("exists")) .thenReturn(Optional.of(dtoFound));

        Result result = applicationController.getSuperhero("exists");

        assertEquals(result.getContentType(), "application/json");

        SuperheroDTO toRender = (SuperheroDTO)result.getRenderable();
        assertSame(dtoFound, toRender);

        assertEquals(result.getStatusCode(), HttpServletResponse.SC_OK);
    }

    @Test
    public void testGetSuperHeroNotFound() {
        when(superheroDao.findSuperheroDTO("not-exists")) .thenReturn(Optional.empty());

        Result result = applicationController.getSuperhero("not-exists");

        assertEquals(result.getContentType(), "application/json");

        assertTrue(((Map)result.getRenderable()).isEmpty());

        assertEquals(result.getStatusCode(), HttpServletResponse.SC_NOT_FOUND);
    }


}
